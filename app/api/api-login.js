class ApiLogin {
    
    Api;
    endpoint = 'login';

    constructor() {
        this.Api = new API();
    }
    login(item) {
        return this.Api.post(this.endpoint, item).then(resp => {
            Auth.setUser(resp.user);
            EventBus.$emit('authAction', true);
        });
    }
}