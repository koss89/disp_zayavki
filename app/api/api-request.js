class ApiRequest {
    
    Api;
    endpoint = 'request';

    constructor() {
        this.Api = new API();
    }

    getAll() {
        return this.Api.getM(this.endpoint);
    }
    getById(id) {
        return this.Api.getM(`${this.endpoint}/${id}`);
    }
    getIncoming() {
        return this.Api.getM(`${this.endpoint}/incoming`);
    }
    find(params) {
        return this.Api.post(`${this.endpoint}/find`, params);
    }
    save(item) {
        return this.Api.post(this.endpoint, item);
    }
    remove(id) {
        return this.Api.delete(`${this.endpoint}/${id}`);
    }
    approve(item) {
        return this.Api.post(`${this.endpoint}/incoming/approve`, item);
    }
}