class ApiComment {
    
    Api;
    endpoint = 'comment';

    constructor() {
        this.Api = new API();
    }

    getAll() {
        return this.Api.getM(this.endpoint);
    }
    getById(id) {
        return this.Api.getM(`${this.endpoint}/${id}`);
    }
    getByRequestId(id) {
        return this.Api.getM(`${this.endpoint}/request/${id}`);
    }
    save(item) {
        return this.Api.post(this.endpoint, item);
    }
    remove(id) {
        return this.Api.delete(`${this.endpoint}/${id}`);
    }
}