class ApiTypework {
    
    Api;
    endpoint = 'typework';

    constructor() {
        this.Api = new API();
    }

    getAll() {
        return this.Api.getM(this.endpoint);
    }
    save(item) {
        return this.Api.post(this.endpoint, item);
    }
}