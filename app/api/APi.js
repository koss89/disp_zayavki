class API {

    basePath = '/api/';

    getM(endpoint) {
        if(cache.hasCache(endpoint)) {
            return new Promise(function (resolve, reject){
                resolve(cache.getCache(endpoint));
            });
        }
        self = this;
        return new Promise(function (resolve, reject){
            axios.get(`${self.basePath}${endpoint}`).then(function(resp) {
                cache.setCache(endpoint, resp.data);
                resolve(resp.data);
            }).catch(function(err) {
                self.handleError(err);
                reject(err);
            })
        });
    }
    post(endpoint, data) {
        if(cache.hasCache(endpoint)) {
            cache.removeCache(endpoint);
        }
        self = this;
        return new Promise(function (resolve, reject){
            axios.post(`${self.basePath}${endpoint}`, data).then(function(resp) {
                Vue.toasted.success( 'Операція успішна!').goAway(3000);
                resolve(resp.data);
            }).catch(function(err) {
                self.handleError(err);
                reject(err);
            })
        });
    }
    delete(endpoint) {
        if(cache.hasCache(endpoint)) {
            cache.removeCache(endpoint);
        }
        self = this;
        return new Promise(function (resolve, reject){
            axios.delete(`${self.basePath}${endpoint}`).then(function(resp) {
                Vue.toasted.success( 'Операція успішна!').goAway(3000);
                resolve(resp.data);
            }).catch(function(err) {
                self.handleError(err);
                reject(err);
            })
        });
    }

    handleError(err) {
        Vue.toasted.error( 'Помилка!').goAway(3000);
        if(err.response.status == 401) {
            router.push('/login');
        }
        console.error(err);
    }
}