class ApiDepartment {
    
    Api;
    endpoint = 'admin/department';

    constructor() {
        this.Api = new API();
    }

    getAll() {
        return this.Api.getM('/department');
    }
    getById(id) {
        return this.Api.getM(`${this.endpoint}/${id}`);
    }
    save(item) {
        return this.Api.post(this.endpoint, item);
    }
    remove(id) {
        return this.Api.delete(`${this.endpoint}/${id}`);
    }
}