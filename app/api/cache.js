class Cache {

  store = {};

  getCache(param) {
    if (this.store.hasOwnProperty(param)) {
      return this.store[param];
    } else {
      return null;
    }
  }
  setCache(param, data) {
    this.store[param] = data;
  }

  removeCache(param) {
    delete this.store[param];
  }

  hasCache(param) {
    return this.store.hasOwnProperty(param);
  }

}

const cache = new Cache();