function t(text) {
  let translation = {
    'DECLINED' : 'Заборонено',
    'CREATED' : 'Створено',
    'APPROVED' : 'Схвалено',
    'OPEN' : 'Відкрито',
    'CLOSED' : 'Закрита',
    'SENDAPPROVE' : 'На погодженні',
    'WORKFINISH' : 'Завершена',
  }

  if(translation[text]) {
    return translation[text];
  } else {
    return text;
  }
}