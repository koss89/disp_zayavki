
class auth {

  user; 

  localStorageUserItemName = 'user';

  constructor() {
    let tmpUser = localStorage.getItem(this.localStorageUserItemName);
    if(tmpUser) {
      this.user = JSON.parse(tmpUser);
    }
  }

  getUser() {
    return this.user;
  }

  setUser(usr) {
    this.user = usr;
    localStorage.setItem(this.localStorageUserItemName, JSON.stringify(usr));
  }

  logout() {
    this.user = undefined;
    localStorage.removeItem(this.localStorageUserItemName);
    EventBus.$emit('authAction', false);
  }

  isAuth() {
    return this.user ? true : false;
  }

  hasAuthority(authority) {
    if(this.isAuth()) {
      return this.user.authority.includes(authority);
    }
    return false;
  }

  isAdmin() {
    return this.hasAuthority('ADMIN');
  }

}

const Auth = new auth();