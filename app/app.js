const router = new VueRouter({
  routes : [
    { path: '/', component: home },
    { path: '/login', component: login },
    { path: '/admin/users', component: AdminUsers },
    { path: '/admin/users/:id', component: AdminUserPage },
    { path: '/admin/departments', component: AdminDepartments },
    { path: '/admin/departments/:id', component: AdminDepartmentPage },
    { path: '/request/:id', component: requestPage },
    { path: '/requests', component: Requests },
    { path: '/oper', component: operPage },

    
  ]
});

Vue.component('date-picker', VueBootstrapDatetimePicker);

Vue.prototype.t = t;
Vue.prototype.moment = moment;

Vue.use(Toasted);

const app = new Vue({
  router
}).$mount('#app')


