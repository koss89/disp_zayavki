const Requests = Vue.component('requests', {
  template: `
  <div>
    <div class="row">
      <div class="col text-center">
        {{requestsType}}
      </div>
    </div>
    <div class="row">
      <div class="col-auto">
        <button @click="loadIncoming" class="btn btn-primary">
          Вхідні
        </button>
      </div>
      <div class="col-auto">
        <button @click="navigateNew" class="btn btn-success">
          Створити заявку
        </button>
      </div>
      <div class="col-md-4 text-center"  v-if="requestsType!='INCOMING'">
        <date-ranges v-model="ranges"></date-ranges>
      </div>
      <div class="col-md-3 text-center"  v-if="requestsType!='INCOMING'">
        <department-select v-model="department"></department-select>
      </div>
      <div class="col-md-2 text-center"  v-if="requestsType!='INCOMING'">
        <status-select v-model="status"></status-select>
      </div>
    </div>
    <div class="row">
      <div class="col">
      </div>
      <div class="col-md-2 text-center"  v-if="requestsType!='INCOMING'">
        <tip-select v-model="tip"></tip-select>
      </div>
      <div class="col text-right">
        <button @click="loadCurrent" class="btn btn-primary">
          Знайти
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <requests-table :value="items" :requests-type="requestsType">
        </requests-table>
      </div>
    </div>
  </div>
  `,
  data: function () {
    return {
      items: [],
      requestsType: 'VOID',
      ranges: {
        dats: moment().format('YYYY-MM-DD ')+'00:00:00',
        datpo: moment().format('YYYY-MM-DD ')+'23:59:59',
      },
      status: '',
      department: '',
      tip: ''
    }
  },
  created: function () {
    api = new ApiRequest();
    api.find(this.ranges).then(data=> {
      this.items = data;
      this.requestsType = "CURRENT";
    });
  },
  methods: {
    loadCurrent: function() {
      api = new ApiRequest();
      let body = {
        dats: this.ranges.dats,
        datpo: this.ranges.datpo,
      };
      if(this.status != '') {
        body.status = this.status;
      }
      if(this.department != '') {
        body.department = this.department;
      }
      if(this.tip != '') {
        body.tip = this.tip;
      }
      api.find(body).then(data=> {
        this.items = data;
        this.requestsType = "CURRENT";
      });
    },
    loadIncoming: function() {
      api = new ApiRequest();
      api.getIncoming().then(data=> {
        this.items = data;
        this.requestsType = "INCOMING";
      });
    },
    navigateNew: function() {
      this.$router.push('/request/new');
    }
  }
})