const login = Vue.component('login', {
    template: `
    <div class="row">
      <div class="col-md-3">
      </div>
      <div class="col-md-6">
          <div class="form-group">
            <label>Логін:</label>
            <input type="text" class="form-control" v-model="item.username">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Пароль:</label>
            <input type="password" class="form-control" v-model="item.password">
          </div>
          <button @click="login" class="btn btn-primary">Вхід</button>
      </div>
      <div class="col-md-3">
      </div>
    </div>
    `,
    data: function () {
      return {
        item: {},
      }
    },
    created: function() {
      Auth.logout();
    },
    methods: {
      login: function () {
        api = new ApiLogin();
        api.login(this.item).then(data => {
          this.$router.push('/');
        });
      },
      
    }
  })