const operPage = Vue.component('operPage', {
  template: `
  <div>
    <div class="row">
      <div class="col-md-3" v-for="item in items">
        <request-card :item="item"></request-card>
      </div>
    </div>

    
  </div>
  `,
  data: function () {
    return {
      items: [],
    }
  },
  created: function () {
    api = new ApiRequest();
    let params  = {};
    params.department = Auth.getUser().departmentid;
    params.dats = moment().format('YYYY-MM-DD ')+'00:00:00';
    params.datpo = moment().format('YYYY-MM-DD ')+'23:59:59';
    api.find(params).then(data=> {
      let paramsOpened  = {};
      paramsOpened.department = Auth.getUser().departmentid;
      paramsOpened.dats = '1900-01-01 00:00:00';
      paramsOpened.datpo = '3000-01-01 23:59:59';
      paramsOpened.status = 'OPEN';
      this.items = [];
      let filtered = this.filterRequests(data);
      this.findAndFilter(paramsOpened, filtered);
      let paramsFinished  = {};
      paramsFinished.department = Auth.getUser().departmentid;
      paramsFinished.dats = '1900-01-01 00:00:00';
      paramsFinished.datpo = '3000-01-01 23:59:59';
      paramsFinished.status = 'WORKFINISH';
      this.findAndFilter(paramsFinished, filtered);
      
    });
  },
  methods: {
    findAndFilter: function (params, data) {
      api = new ApiRequest();
      api.find(params).then(resp=> {
        items = this.items;
        resp.forEach(el => {
          ff = data.filter(f => f.id===el.id);
          if(ff.length==0) {
            items.push(el);
          }
        });
        this.items =  [].concat(items, data);
      });
    },
    save: function () {
      api = new ApiRequest();
      api.save(this.item).then(data => {
        this.$router.push('/');
      });
    },
    sendApprove: function() {
      let api = new ApiApprove();
      let allPromises = [];
      if(this.approveDepartments.length>0) {
        this.approveDepartments.forEach(element => {
          let approve = {
            requestid: this.item.id,
            departmentid: element.id
          }
          allPromises.push(api.save(approve));
        });
        let self = this;
        Promise.all(allPromises).then(function() {
          self.approveDialog = false;
        });
      }
    },
    filterRequests: function(items) {
      return items.filter(item => {
        let flag= true;
        if(item.status=='DECLINED' || item.status=='CREATED') {
          flag= false;
        }
        return flag;
      })
    }
    
  }
})