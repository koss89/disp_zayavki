const requestPage = Vue.component('requestPage', {
  template: `
  <div>
    <div class="row" v-if="!isNew">
      <div class="col">
        Дата створення: {{item.created}}
      </div>
      <div class="col">
        Статус:<status-info :value="item.status"></status-info>
      </div>
    </div>
    <hr />
    <div class="row">
      <div class="col-md-8">
        <form @submit="save">
          <div class="row">
            <div class="col">
              <tip-select v-model="item.tip"></tip-select>
            </div>
            <div class="col">
                <label># Наряду:</label>
                <input type="text" class="form-control" v-model="item.narad">
            </div>
          </div>
          <br />
          <div class="row">
            <div class="col">
              <department-select :disabled="!isNew" v-model="item.departmentid"></department-select>
            </div>
            <div class="col">
              <typework-select v-model="item.typeworkid"></typework-select>
            </div>
          </div>
          <div class="form-group">
            <label>Підстанція:</label>
            <input type="text" class="form-control" v-model="item.ps">
          </div>
          <div class="form-group">
            <label>Обладнання:</label>
            <input type="text" class="form-control" v-model="item.equipment">
          </div>
          <div class="form-group">
            <label>Опис:</label>
            <input type="text" class="form-control" v-model="item.description">
          </div>
          <div class="form-group">
            <label>Технічні заходи:</label>
            <input type="text" class="form-control" v-model="item.tehzah">
          </div>
          <div class="row">
            <div class="col">
              <div class="form-group">
                <label>Дата початку:</label>
                <date-picker v-model="item.dats" :config="{format: 'YYYY-MM-DD HH:mm:ss'}"></date-picker>
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label>Дата закінчення:</label>
                <date-picker v-model="item.datpo" :config="{format: 'YYYY-MM-DD HH:mm:ss'}"></date-picker>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col text-center">
              <button type="submit" v-if="isNew || item.status=='CREATED'" class="btn btn-primary">Зберегти</button>
            </div>
          </div>
        </form>
        <button v-if="!isNew" class="btn btn-primary" @click="approveDialog = true">Надіслати на погодження</button>
      </div>
      <div class="col-md-4">
        <approve-list :items="item.approve"></approve-list>
        <hr />
        <h4 class="text-center">Фактичний час:</h4>
        <fakttime-card :item="item"></fakttime-card>
      </div>
    </div>
    <div class="row" v-if="!isNew">
      <div class="col">
        <comment-list v-bind:requestid="item.id"></comment-list>
      </div>
    </div>
    <approve-dialog v-if="!isNew" :show="approveDialog" v-model="approveDepartments" @send="sendApprove" @close="approveDialog = false"></approve-dialog>
  </div>
  `,
  data: function () {
    return {
      item: {},
      isNew: true,
      approveDialog: false,
      approveDepartments: []
    }
  },
  created: function () {
    api = new ApiRequest();
    if(this.$route.params.id && this.$route.params.id != 'new') {
      this.isNew = false;
      api.getById(this.$route.params.id).then(data=> {
        this.item = data;
      });
    } else {
      this.item = {
        status: "CREATED"
      };
    }
  },
  methods: {
    save: function () {
      api = new ApiRequest();
      api.save(this.item).then(data => {
        this.$router.push('/');
      });
    },
    sendApprove: function() {
      let api = new ApiApprove();
      let allPromises = [];
      if(this.approveDepartments.length>0) {
        this.approveDepartments.forEach(element => {
          let approve = {
            requestid: this.item.id,
            departmentid: element.id
          }
          allPromises.push(api.save(approve));
        });
        let self = this;
        Promise.all(allPromises).then(function() {
          self.approveDialog = false;
        });
        
      }
    }
    
  }
})