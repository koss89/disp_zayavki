const AdminUserPage = Vue.component('AdminUserPage', {
  template: `
    <div class="row">
      <div class="col">
        <form @submit="save">
          <div class="form-group">
            <label for="exampleInputEmail1">User name:</label>
            <input type="text" class="form-control" v-model="item.username">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">password:</label>
            <input type="password" class="form-control" v-model="item.password">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">FIO:</label>
            <input type="text" class="form-control" v-model="item.fio">
          </div>
          <department-select v-model="item.departmentid"></department-select>
          <button type="submit" class="btn btn-primary">Save</button>
        </form>
      </div>
    </div>`,
  data: function () {
    return {
      item: {},
    }
  },
  created: function () {
    api = new ApiAdminUsers();
    if(this.$route.params.id && this.$route.params.id != 'new') {
      api.getById(this.$route.params.id).then(data=> {
        this.item = data;
      });
    } else {
      this.item = {
        departmentid: '1'
      };
    }
  },
  methods: {
    save: function () {
      api = new ApiAdminUsers();
      api.save(this.item).then(data => {
        this.$router.push('/admin/users');
      });
    },
    
  }
})