const AdminUsers = Vue.component('adminUsers', {
  template: `
  <div>
    <div class="row">
      <div class="col">
      <router-link :to="'/admin/users/new'" class="btn btn-link">create</router-link>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <table class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Username</th>
              <th>fio</th>
              <th>active</th>
              <th>authority</th>
              <th>actions</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="item in items">
              <td><router-link :to="'/admin/users/'+item.id">{{item.id}}</router-link></td>
              <td>{{item.username}}</td>
              <td>{{item.fio}}</td>
              <td>{{item.active}}</td>
              <td>{{item.authority}}</td>
              <td>
                <button class="btn btn-sm btn-danger" @click="remove(item.id)">
                  rem
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>`,
  data: function () {
    return {
      items: []
    }
  },
  created: function () {
    api = new ApiAdminUsers();
    api.getAll().then(data=> {
      console.log(data);
      this.items = data;
    });
  },
  methods: {
    encrement: function () {
      this.obj.val = this.obj.val+1;
    },
    remove(id) {
      api = new ApiAdminUsers();
      api.remove(id).then(() => {
        api.getAll().then(data=> {
          this.items = data;
        });
      });

    }

    
  }
})