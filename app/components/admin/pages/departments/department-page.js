const AdminDepartmentPage = Vue.component('AdminDepartmentPage', {
  template: `
    <div class="row">
      <div class="col">
        <form @submit="save">
          <div class="form-group">
            <label for="exampleInputEmail1">Name:</label>
            <input type="text" class="form-control" v-model="item.nam">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">lvl:</label>
            <input type="number" class="form-control" v-model="item.lvl">
          </div>
          <button type="submit" class="btn btn-primary">Save</button>
        </form>
      </div>
    </div>`,
  data: function () {
    return {
      item: {},
    }
  },
  created: function () {
    api = new ApiDepartment();
    if(this.$route.params.id && this.$route.params.id != 'new') {
      api.getById(this.$route.params.id).then(data=> {
        this.item = data;
      });
    } else {
      this.item = {};
    }
  },
  methods: {
    save: function () {
      api = new ApiDepartment();
      api.save(this.item).then(data => {
        this.$router.push('/admin/departments');
      });
    },
    
  }
})