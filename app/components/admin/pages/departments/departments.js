const AdminDepartments = Vue.component('adminDepartments', {
  template: `
  <div>
    <div class="row">
      <div class="col">
      <router-link :to="'/admin/departments/new'" class="btn btn-link">create</router-link>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <table class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>name</th>
              <th>lvl</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="item in items">
              <td><router-link :to="'/admin/departments/'+item.id">{{item.id}}</router-link></td>
              <td>{{item.nam}}</td>
              <td>{{item.lvl}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>`,
  data: function () {
    return {
      items: []
    }
  },
  created: function () {
    api = new ApiDepartment();
    api.getAll().then(data=> {
      this.items = data;
    });
  },
  methods: {
    encrement: function () {
      this.obj.val = this.obj.val+1;
    },
    remove(id) {
      api = new ApiDepartment();
      api.remove(id).then(() => {
        api.getAll().then(data=> {
          this.items = data;
        });
      });

    }

    
  }
})