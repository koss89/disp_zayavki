Vue.component('tipSelect', {
  template: `
  <div class="row">
    <div class="col-auto">
      <label>Тип:</label>
    </div>
    <div class="col">
      <select :disabled="disabled"  class="form-control" v-on:input="updateValue($event.target.value)" :value="value" >
        <option class="text-info" value="1">Планова</option>
        <option class="text-warning" value="2">Аварійна</option>
      </select>
    </div>
  </div>
  `,
  props: {
    value: {
      type: String,
      default: '1'
    },
    disabled:  {
      type: Boolean,
      default: false
    },
  },
  methods: {
    updateValue: function (newValue) {
      if(this.value != newValue) {
        this.$emit('input', newValue);
      }
    }
  }
});