Vue.component('statusSelect', {
  template: `
  <div class="row">
    <div class="col-auto">
      <label>Статус:</label>
    </div>
    <div class="col">
      <select :disabled="disabled"  class="form-control" v-on:input="updateValue($event.target.value)" :value="value" >
        <option class="text-secondary" value="CREATED">Створено</option>
        <option class="text-info" value="SENDAPPROVE">На погодженні</option>
        <option class="text-primary" value="OPEN">Відкрито</option>
        <option class="text-primary" value="WORKFINISH">Роботи закінчено</option>
        <option class="text-success" value="APPROVED">Схвалено</option>
        <option class="text-warning" value="CLOSED">Закрита</option>
        <option class="text-danger" value="DECLINED">Відхилено</option>
      </select>
    </div>
  </div>
  `,
  props: {
    value: {
      type: String,
      default: ''
    },
    disabled:  {
      type: Boolean,
      default: false
    },
  },
  methods: {
    updateValue: function (newValue) {
      if(this.value != newValue) {
        this.$emit('input', newValue);
      }
    }
  }
});