Vue.component('approveList', {
  template: `
  <div v-if="items">
    <div class="row" v-for="item in items">
      <div class="col">{{moment(item.dat).format('DD.MM.YY hh:mm')}}</div>
      <div class="col" v-bind:class="{ 'text-danger': item.status=='DECLINED', 'text-success': item.status=='APPROVED' }">{{t(item.status)}}</div>
      <div class="col">{{getDepartment(item.departmentid).nam}}</div>
    </div>
  </div>
  `,
  data: function () {
    return {
      departments: []
    }
  },
  props: {
    items:  {
      type: Array,
      default: () => ([])
    },
  },
  created: function () {
    api = new ApiDepartment();
    api.getAll().then(data=> {
      this.departments = data;
    });
  },
  methods: {
    getDepartment: function(id) {
      let result =  this.departments.find(el => el.id==id);
      return result || { nam:''}
    }
  }
})
