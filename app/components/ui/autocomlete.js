Vue.component('autocomplete', {
  template: `
  <div class="row">
    <div class="col">
    <input class="form-control" :list="id" @change="inputHandler" v-model="val">
    <datalist :id="id">
      <option v-for="item in options" :value="item"></option>
    </datalist>
    </div>
  </div>
  `,
  data: function () {
    return {
      id: new Date(),
      val: ''
    }
  },
  props: {
    value:  {
      type: String,
      default: ""
    },
    options:  {
      type: Array,
      default: () => ([])
    },
  },
  created: function() {
    this.val= this.value;
  },
  methods: {
    inputHandler: function(evt) {
      this.$emit("input", this.val);
    }
  }
})
