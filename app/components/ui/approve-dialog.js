Vue.component('approveDialog', {
  template: `
  <div v-if="show">
    <modal @close="onClose">
        <h3 slot="header">Надіслати на погодження</h3>
        <div slot="body">
          <div class="form-group">
            <label>Фільтр:</label>
            <input class="form-control" v-model="filterName">
          </div>
          <div v-for="item in filteredItems">
            <div v-if="!selected.includes(item)" class="row">
              <div class="col">
                <span>{{item.nam}}</span>
              </div>
              <div class="col">
                <button class="btn btn-sm btn-primary" @click="add(item)">Додати</button>
              </div>
            </div>
          </div>
          <hr />
          <div v-for="item in selected">
            <div v-if="selected.includes(item)" class="row">
              <div class="col">
                <span>{{item.nam}}</span>
              </div>
              <div class="col">
                <button class="btn btn-sm btn-danger" @click="remove(item)">Видалити</button>
              </div>
            </div>
          </div>
          <div class="text-center"><button :disabled="selected.length==0" class="btn btn-warning" @click="$emit('send')">Відправити</button></div>
        </div>
    </modal>
  </div>
  `,
  data: function () {
    return {
      items:[],
      selected: [],
      filterName: ''
    }
  },
  props: {
    show: false
  },
  created: function () {
    api = new ApiDepartment();
    api.getAll().then(data=> {
      this.items = data;
    });
  },
  methods: {
    onClose: function () {
      this.$emit('close');
    },
    add(item) {
      if(!this.selected.includes(item)) {
        this.selected.push(item);  
        this.$emit('input', this.selected);
      }
    },
    remove(item) {
      if(this.selected.includes(item)) {
        const index = this.selected.indexOf(item);
        if (index > -1) {
          this.selected.splice(index, 1);
        }  
        this.$emit('input', this.selected);
      }
    },
  },
  computed: {
    filteredItems: function() {
      if(this.filterName!= '') {
        return this.items.filter(item => item.nam.includes(this.filterName));
      } else {
        return this.items;
      }
    }
  }
})
