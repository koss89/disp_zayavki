Vue.component('departmentSelect', {
  template: `
  <div class="row">
    <div class="col-auto">
      Підрозділ:
    </div>
    <div class="col">
      <select :disabled="disabled" class="form-control" v-on:input="updateValue($event.target.value)" :value="value">
        <option value=""> </option>
        <option v-for="item in items" :value="item.id" :selected="item.id == value">{{item.nam}}</option>
      </select>
    </div>
  </div>
  `,
  data: function () {
    return {
      items: []
    }
  },
  props: {
    value: {
      type: String,
      default: ''
    },
    disabled:  {
      type: Boolean,
      default: false
    },
  },
  created: function () {
    api = new ApiDepartment();
    api.getAll().then(data=> {
      this.items = data;
    });
  },
  methods: {
    updateValue: function (newValue) {
      if(this.value != newValue) {
        this.$emit('input', newValue);
      }
    }
  }
});