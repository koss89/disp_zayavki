Vue.component('requestCard', {
  template: `
  <div class="request-card">
  <div v-if="item" class="border rounded" v-bind:class="{ 'border-danger': item.tip==2, 'border-info': item.tip==1 }">
    <div class="row">
      <div class="col-auto">
        <h4>#{{item.id}}</h4>
      </div>
      <div class="col">
        <h4>
          <span v-if="item.tip==1" class="text-info">Планова</span>
          <span v-if="item.tip==2" class="text-warning">Аварійна</span>
        </h4>
      </div>
      <div class="col text-right">
        <h4><status-info :value="item.status"></status-info></h4>
      </div>
    </div>
    <hr />
    <div class="row">
      <div class="col-auto">
        <b>З:</b>
        <span>{{item.dats}}</span>
      </div>
      <div class="col-auto text-right">
        <b>По:</b>
        <span>{{item.datpo}}</span>
      </div>
    </div>
    <hr />
    <div class="row">
      <div class="col">
        <b>Підстанція:</b>
        <span>{{item.ps}}</span>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <b>Обладнання:</b>
        <span>{{item.equipment}}</span>
      </div>
    </div>
    <hr />
    <div class="row" v-if="item.status!='CLOSED'">
      <div class="col" v-if="item.status!='OPEN' && item.status!='CLOSED'  && item.status!='WORKFINISH'">
        <button class="btn btn-outline-primary" @click="openRequest">Відкрити</button>
      </div>
      <div class="col" v-if="item.status=='OPEN'">
        <button class="btn btn-outline-primary" @click="finishRequest">Закінчити роботи</button>
      </div>
      <div class="col text-right" v-if="item.status=='WORKFINISH'">
        <button class="btn btn-outline-danger" @click="closeRequest">Закрити</button>
      </div>
    </div>
  </div>
  </div>
  `,
  data: function () {
    return {
      departments: []
    }
  },
  props: {
    item:  {
      type: Object,
      default: () => ({})
    },
  },
  methods: {
    getDepartment: function(id) {
      let result =  this.departments.find(el => el.id==id);
      return result || { nam:''}
    },
    openRequest: function() {
      api = new ApiRequest();
      let request = {
        id: this.item.id,
        status: "OPEN"
      };
      api.save(request).then(data => {
        this.item.status = "OPEN";
      });
    },
    finishRequest: function() {
      api = new ApiRequest();
      let request = {
        id: this.item.id,
        status: "WORKFINISH"
      };
      api.save(request).then(data => {
        this.item.status = "WORKFINISH";
      });
    },
    closeRequest: function() {
      api = new ApiRequest();
      let request = {
        id: this.item.id,
        status: "CLOSED"
      };
      api.save(request).then(data => {
        this.item.status = "CLOSED";
        //window.location.reload();
      });
    }

  }
})
