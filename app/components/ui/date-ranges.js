Vue.component('dateRanges', {
    template: `
    <div class="row">
        <div class="col-auto">   
            З:
        </div>
        <div class="col">
            <date-picker v-model="range.dats" :config="{format: 'YYYY-MM-DD HH:mm:ss'}" @dp-change="onChange"></date-picker>
        </div>
        <div class="col-auto">   
            По:
        </div>
        <div class="col">
            <date-picker v-model="range.datpo" :config="{format: 'YYYY-MM-DD HH:mm:ss'}" @dp-change="onChange"></date-picker>
        </div>
    </div>
    `,
    data: function () {
      return {
        range: {
            dats: moment().format('YYYY-MM-DD ')+'00:00:00',
            datpo: moment().format('YYYY-MM-DD ')+'23:59:59',
        }
      }
    },
    created: function () {},
    methods: {
        onChange: function() {
            this.$emit('input', this.range);
        }
    }
  });