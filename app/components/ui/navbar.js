const Navbar = Vue.component('navbar', {
  template: `
  <nav class="navbar navbar-expand-lg navbar-light bg-light" v-if="logged">
    <a class="navbar-brand" href="/">Головна</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="/#/oper">Оперативно</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/#/requests">Заявки</a>
        </li>
        <li class="nav-item dropdown" v-if="isAdmin">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Admin
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/#/admin/users">Users</a>
            <a class="dropdown-item" href="/#/admin/departments">Departments</a>
          </div>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="nav-item">
          <a class="btn btn-warrning" href="/#/login">Вихід</a>
        </li>
      </ul>
    </div>
  </nav>
  `,
  data: function () {
    return {
      logged: Auth.isAuth(),
    }
  },
  created: function() {
    let self = this;
    EventBus.$on('authAction', (logged) => {
      this.logged = logged;
      self.$forceUpdate();
    });
  },
  computed: {
    isAdmin() {
      return Auth.isAdmin();
    },
  }
  });