Vue.component('statusInfo', {
  template: `
  <span  v-bind:class="{ 'text-secondary': value=='CREATED', 'text-success': value=='APPROVED', 'text-danger': value=='DECLINED'}">
    {{t(value)}}
  </span>
  `,
  props: {
    value: {
      type: String,
      default: ''
    },
  },
});