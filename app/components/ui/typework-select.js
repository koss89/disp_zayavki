Vue.component('typeworkSelect', {
  template: `
  <div class="row">
    <div class="col-auto">
      <label>Вид робіт:</label>
    </div>
    <div class="col">
      <select :disabled="disabled"  class="form-control" v-on:input="updateValue($event.target.value)" :value="value" >
      <option :value="null">---</option>
        <option v-for="item in items" :value="item.id" :selected="item.id == value">{{item.nam}}</option>
      </select>
    </div>
  </div>
  `,
  props: {
    value: {
      type: String,
      default: ''
    },
    disabled:  {
      type: Boolean,
      default: false
    },
  },
  data: function () {
    return {
      items: []
    }
  },
  created: function () {
    api = new ApiTypework();
    api.getAll().then(data=> {
      this.items = data;
    });
  },
  methods: {
    updateValue: function (newValue) {
      if(this.value != newValue) {
        this.$emit('input', newValue);
      }
    }
  }
});