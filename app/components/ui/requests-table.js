const RequestsTable = Vue.component('requestsTable', {
  template: `
  <div class="row">
    <div class="col">
      <table class="table text-center">
        <thead>
          <tr>
            <th>#</th>
            <th>Тип</th>
            <th>Статус</th>
            <th>Підрозділ</th>
            <th>Початок</th>
            <th>Кінець</th>
            <th>Фактично</th>
            <th style="width: 15%">Підстанція</th>
            <th style="width: 15%">Обладнання</th>
            <th  style="width: 15%">Опис</th>
            <th>Створено</th>
            <th v-if="requestsType=='INCOMING'">Схвалення</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="item in value">
            <td><router-link :to="'/request/'+item.id">{{item.id}}</router-link></td>
            <td>
              <span v-if="item.tip==1" class="text-info">Планова</span>
              <span v-if="item.tip==2" class="text-warning">Аварійна</span>
            </td>
            <td><status-info :value="item.status"></status-info></td>
            <td>{{item.department.nam}}</td>
            <td>{{moment(item.dats).format('DD.MM.YY hh:mm')}}</td>
            <td>{{moment(item.datpo).format('DD.MM.YY hh:mm')}}</td>
            <td>
              <span v-if="item.fakts">{{moment(item.fakts).format('DD.MM.YY hh:mm')}}</span>
              <span>-</span>
              <span v-if="item.faktpo">{{moment(item.faktpo).format('DD.MM.YY hh:mm')}}</span>
            </td>
            <td>{{item.ps}}</td>
            <td>{{item.equipment}}</td>
            <td>{{item.description}}</td>
            <td>{{item.user.department.nam}}</td>
            <td v-if="requestsType=='INCOMING'">
              <button class="btn btn-sm btn-success" @click="approve('APPROVED', item.id)">Схвалити</button>
              <button class="btn btn-sm btn-danger" @click="approve('DECLINED', item.id)">Відхилити</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  `,
  props: {
    value: {
      type: Array,
      default: []
    },
    requestsType: {
      type : String,
      default: ''
    }
  },
  methods: {
    updateValue: function (newValue) {
      if(this.value != newValue) {
        this.$emit('input', newValue);
      }
    },
    approve: function(status, id) {
      const approve = {
        requestid: id,
        status
      };
      let api = new ApiRequest();

      api.approve(approve).then(()=> {
      });


    }
  }
});