const CommentEditor = Vue.component('commentEditor', {
  template: `
  <div>
    <div class="row">
      <div class="col">
        <div class="form-group">
          <label>Введыть коментар:</label>
          <textarea type="text" class="form-control h100" v-model="item.comment"></textarea>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col text-center">
        <button class="btn btn-success" @click="save">Додати коментар</button>
      </div>
    </div>
  </div>
  `,
  props: {
    requestid: null
  },
  data: function () {
    return {
      item: {},
    }
  },
  methods: {
    save: function () {
      this.item.requestid = this.requestid;
      console.log( JSON.stringify(this.item));
      api = new ApiComment();
      api.save(this.item).then(data => {
        //this.$router.push('/');
      });
    },
  }
})