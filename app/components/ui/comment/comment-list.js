const CommentList = Vue.component('commentList', {
  template: `
  <div>
    <div class="row">
      <div class="col">
      <comment-editor v-bind:requestid="requestid"></comment-editor>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <comment v-for="item in items" v-bind:key="item.id" v-bind:comment="item"></comment>
      </div>
    </div>
  </div>
  `,
  data: function () {
    return {
      items: [],
    }
  },
  props: {
    requestid: null
  },
  created: function () {
   
  },
  watch: { 
    requestid: function(newVal, oldVal) {
      if(newVal != oldVal) {
        api = new ApiComment();
        api.getByRequestId(this.requestid).then( data =>{
          this.items = data;
        });
      }
    }
  }
})