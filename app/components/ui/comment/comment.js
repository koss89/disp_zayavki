const Comment = Vue.component('comment', {
  template: `
  <div>
    <div class="row">
      <div class="col">
        <b>Дата:</b>{{comment.created}}
      </div>
      <div class="col">
        <b>Користувач:</b>{{comment.user.fio}}
      </div>
    </div>
    <div class="row">
      <div class="col">
        {{comment.comment}}
      </div>
    </div>
    <hr />
  </div>
  `,
  props: {
    comment: {}
  },
})