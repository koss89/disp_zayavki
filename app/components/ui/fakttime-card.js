Vue.component('fakttimeCard', {
  template: `
  <div v-if="item && item.fakts && item.faktpo">
    <div class="row">
      <div class="col">
        <b>Початок:</b>
        {{moment(item.fakts).format('DD.MM.YY hh:mm')}}
      </div>
      <div class="col">
        <b>Закінчення:</b>
        {{moment(item.faktpo).format('DD.MM.YY hh:mm')}}
      </div>
    </div>
  </div>
  `,
  data: function () {
    return {
      departments: []
    }
  },
  props: {
    item:  {
      type: Object
    },
  },
})
