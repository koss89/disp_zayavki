<?php

function zay_autoload($class)
{ 
  //echo $class.'<br />';
  $realparh = realpath(dirname(__FILE__));

  $namespace = preg_replace('/^zay/', "", $class);
  $namespace = str_replace("\\", DIRECTORY_SEPARATOR, $namespace);
  $path = $realparh . $namespace . '.php';

  require_once $path;
}

spl_autoload_register("zay_autoload");
