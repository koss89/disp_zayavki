<?php

namespace zay;

require_once __DIR__.'/autoload.php';

use zay\Utils\Exceptions\NotAuthException;
use zay\Utils\Exceptions\AccesDenyException;

class Api {

  public function __construct() {
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    try {
      $router = new Router();
     
      $result = $router->Route();
      $json = json_encode($result);
      echo $json;
      
    
    } catch(NotAuthException $exception) {
      http_response_code(401);
      echo '{ "result": "error", "message":"'.$exception->getMessage().'", "code":'.$exception->getCode().' }';
    } catch(AccesDenyException $exception) {
      http_response_code(403);
      echo '{ "result": "error", "message":"'.$exception->getMessage().'", "code":'.$exception->getCode().' }';
    } catch(\Exception $exception) {
      http_response_code(500);
      echo '{ "result": "error", "message":"'.$exception->getMessage().'", "code":'.$exception->getCode().' }';
    }
  }

}

new Api();
