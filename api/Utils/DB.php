<?php

namespace zay\Utils;

use zay\Config;

class DB {

  private static $instance;
  private $config;
  private $connection;


  public function __construct() {
    if(!self::$instance) {
      $co = Config::getInstance();
      $this->config = $co->getConfig("db");
      self::$instance = $this;
    }
  }

  public static function getInstance() {
    if(!self::$instance) {
      new DB();
    }
    return self::$instance;
  }

  public function connect() {
    if (!$this->connection instanceof \PDO) {
      $this->connection = new \PDO(
          'mysql:dbname=' . $this->config['db'] . ';host=' . $this->config['host'] . ';port=' . $this->config['port'],
          $this->config['user'],
          $this->config['pass'],
          array(\PDO::ATTR_PERSISTENT => true)
      );
      $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION, \PDO::PARAM_BOOL);
    }
  }

  public function getDB() {
    $this->connect();
    return $this->connection;
  }

  private function query(string $query, array $params = []) {
    $db = $this->getDB();
    $stmt = $db->prepare($query);

    foreach ($params as $param) {
      $typ = \PDO::PARAM_STR;
      if (is_int($param[1])) {
        $typ = \PDO::PARAM_INT;
      }
      $stmt->bindParam($param[0], $param[1], $typ);
    }
    $stmt->execute();
    return $stmt;
  }

  public function sql(string $query) {
    $db = $this->getDB();
    return $db->exec($query);
  }

  public function select(string $query, array $params = []) {
    $st = $this->query($query, $params);
    return $st->fetchAll(\PDO::FETCH_ASSOC);
  }

  public function exec(string $query, array $params = []) {
    $st = $this->query($query, $params);
    return $st->rowCount();
  }
}
