<?php

namespace zay\Utils\Exceptions;

class AccesDenyException extends AppException {

  public function __construct() {
    parent::__construct("Acces Deny", 10);
  }
}
