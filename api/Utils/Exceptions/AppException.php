<?php

namespace zay\Utils\Exceptions;

class AppException extends \Exception {

  public function __construct($message="", $code=0) {
    $code = 10000 + $code;
    parent::__construct($message, $code);
  }

}
