<?php

namespace zay\Utils\Exceptions;

class NotAuthException extends AppException {

  public function __construct() {
    parent::__construct("User not authorize", 1);
  }
}
