<?php

namespace zay;

class Config {

  private static $configInstance;
  private $config;

  public function __construct() {
    if(!self::$configInstance) {
      $this->setConfig();
      self::$configInstance = $this;
    }
  }

  public static function getInstance() {
    if(!self::$configInstance) {
      new Config();
    }

    return self::$configInstance;
  }

  private function setConfig() {
    $this->config = [
      "db" => [
        "host" => "localhost",
        "port" => "3306",
        "db" => "zay",
        "user" => "zay",
        "pass" => "zay"
      ]
    ];
  }

  public function getConfig($part = NULL) {
    if($part) {
      return $this->config[$part];
    }
    return $this->config;
  }


}
