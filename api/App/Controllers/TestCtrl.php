<?php

namespace zay\App\Controllers;

use zay\App\Services\UserServ;
use zay\App\Auth;
use zay\Utils\Exceptions\UserExsistException;
use zay\App\Services\ReadingServ;
use zay\App\Services\TreatmentServ;

class TestCtrl extends AbstractCtrl {

    public function testMe() {
        return [
            'first' => 'fdsdfsdf',
            'secondArr' => [
                'dfgsdfs',
                'dfsdfsdfds'
            ],
            'threeAsocArrr' => 'dsgfsdgsdds',
            'dsfsdfsd' => 1
        ];
    }


    public function echoMe() {
        return $this->getBody();
    }

  
}
