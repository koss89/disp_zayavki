<?php

namespace zay\App\Controllers;

use zay\App\Services\RequestServ;
use zay\App\Auth;
use zay\App\Services\UserServ;
use zay\App\Services\DepartmentServ;
use zay\App\Services\ApprovesServ;
use zay\App\Services\HistoryRequestStatusServ;


class RequestCtrl extends AbstractCtrl {

  private $service;

  public function __construct() {
    $this->service = new RequestServ();
  }

  public function getAll() {
    $items = $this->service->getAll();
    return $this->loadAdditionalInfo($items);
  }

  public function find() {
    $auth = new Auth();
    $user = $auth->getCurrentUser();
    $body = $this->getBody();
    $items = $this->service->find($body);
    return $this->loadAdditionalInfo($items);
  }

  public function incoming() {
    $auth = new Auth();
    $user = $auth->getCurrentUser();
    $items = $this->service->getIncoming($user['departmentid']);
    $items = $this->loadAdditionalInfo($items);
    $filteredItems = [];
    $departmentService = new DepartmentServ();
    $currentUserDepartment = $departmentService->getById($user['departmentid']);
    foreach($items as $item) {
      $flag = true;
      foreach($item['approve'] as $approve) {
        $approveDepartment = $departmentService->getById($approve['departmentid']);
        if(!$approve['userid'] && $currentUserDepartment['lvl']<$approveDepartment['lvl']) {
          $flag = false;
          break;
        }
      }
      if($flag == true) {
        $filteredItems[] = $item;
      }
    }
    return $filteredItems;
  }

  public function save() {
    $body = $this->getBody();
    unset($body['user']);
    unset($body['department']);
    unset($body['approve']);
    $auth = new Auth();
    $user = $auth->getCurrentUser();
    if(!array_key_exists('id', $body)) {
      $body['userid'] = $user['id'];
    } else {
      if($body['status'] !== $oldRequest['status']) {
        $historyRequestStatusService = new HistoryRequestStatusServ();
        $historyRequestStatusService->logHistory($body['id'], $user['id'], $body['status']);
      }
      $oldRequest = $this->service->getById($body['id']);
      if($body['status']=="OPEN" && !$oldRequest['fakts']) {
        $body['fakts'] = date("Y-m-d H:i:s");
      }
      if($body['status']=="CLOSED" && !$oldRequest['faktpo']) {
        $body['faktpo'] = date("Y-m-d H:i:s");
      }
    }
    return $this->service->save($body);
  }

  public function getById() {
    $requestUri = $_REQUEST['path'];
    $id = preg_replace('/^request\//','',$requestUri);
    $request =  $this->service->getById($id);
    $request = $this->loadApproves($request);
    $request = $this->loadUser($request);
    return $request;
  }

  public function loadAdditionalInfo($items) {
    $userService = new UserServ();
    $departmentService = new DepartmentServ();
    $approveService = new ApprovesServ();
    foreach($items as $key => $item) {
      $items[$key]['user'] = $userService->getUserWichDepartment($item['userid']);
      $items[$key]['department'] = $departmentService->getById($item['departmentid']);
      $items[$key]['approve'] = $approveService->getByRequestId($item['id']);
      foreach($items[$key]['approve'] as $akey =>$app) {
        if($app['userid']) {
          $items[$key]['approve'][$akey]['user'] = $userService->getById($app['userid']);
        }
      }
    }
    return $items;
  }

  public function loadApproves($item) {
    $approveService = new ApprovesServ();
    $item['approve'] = $approveService->getByRequestId($item['id']);
    return $item;
  }

  public function loadUser($item) {
    $userService = new UserServ();
    $item['user'] = $userService->getUserWichDepartment($item['userid']);
    return $item;
  }

}
