<?php

namespace zay\App\Controllers;

class AllowOriginCtrl {
    
    public function __construct() {
    }
  
    public function allow() {
      header('Access-Control-Allow-Credentials: true');
      header('Access-Control-Allow-Methods: DELETE, POST, GET, OPTIONS');
      header('Access-Control-Allow-Headers: *');
      header('Access-Control-Max-Age: 3600');
      exit(0);
    }
  

}