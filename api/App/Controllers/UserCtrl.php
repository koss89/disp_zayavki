<?php

namespace zay\App\Controllers;

use zay\App\Services\UserServ;
use zay\App\Auth;
use zay\Utils\Exceptions\UserExsistException;
use zay\App\Services\ReadingServ;
use zay\App\Services\TreatmentServ;

class UserCtrl extends AbstractCtrl {

  private $service;

  public function __construct() {
    $this->service = new UserServ();
  }
  
  public function create() {
    $body = $this->getBody();
    unset($body['id']);
    $usersUsername = $this->service->getAllByField("username", $body["username"]);
    if(count($usersUsername)>0) {
      throw new UserExsistException();
    }
    $result = $this->service->save($body);
    return ["status" => "OK", "message" => "User created"];
  }

  public function edit() {
    $user = $this->getBody();
    $oldUser = $this->service->getById($user['id']);
    $result = $this->service->save($user);
    $editedUser = $this->service->getById($user['id']);
  
    return ["status" => "OK", "message" => "User edited"];
  }

  public function save() {
    $body = $this->getBody();
    if(array_key_exists('id', $body)) {
      return $this->edit();
    } else {
      return $this->create();
    }
  }

  public function getAll() {
      return $this->service->getAll();
  }

  public function getById() {
    $requestUri = $_REQUEST['path'];
    $id = preg_replace('/^admin\/user\//','',$requestUri);
    return $this->service->getById($id);
  }
  public function removeById() {
    $requestUri = $_REQUEST['path'];
    $id = preg_replace('/^admin\/user\//','',$requestUri);
    $user = $this->service->getById($id);
    $user['active'] = '0';
    $this->service->save($user);
    return ["status" => "OK", "message" => "User removed"];
  }
  
}
