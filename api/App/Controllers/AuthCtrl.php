<?php

namespace zay\App\Controllers;

use zay\App\Services\UserServ;
use zay\App\Auth;
use zay\Utils\Exceptions\UserExsistException;
use zay\App\Services\ReadingServ;
use zay\App\Services\TreatmentServ;

class AuthCtrl extends AbstractCtrl {

  private $auth;

  public function __construct() {
    $this->auth = new Auth();
  }

  public function login() {
    $cred = $this->getBody();
    return $this->auth->Auth($cred);
  }
}
