<?php

namespace zay\App\Controllers;

use zay\App\Request;

abstract class AbstractCtrl {

  public function getBody() {
    $rawInput = file_get_contents('php://input');
    return json_decode($rawInput, TRUE);
  }
}
