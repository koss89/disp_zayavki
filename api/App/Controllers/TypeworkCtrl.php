<?php

namespace zay\App\Controllers;

use zay\App\Services\TypeworkServ;

class TypeworkCtrl extends AbstractCtrl {

  private $service;

  public function __construct() {
    $this->service = new TypeworkServ();
  }

  public function getAll() {
    return $this->service->getAll();
  }

  public function save() {
    $body = $this->getBody();
    return $this->service->save($body);
  }

}
