<?php

namespace zay\App\Controllers;

use zay\App\Services\UserServ;
use zay\App\Auth;
use zay\Utils\Exceptions\UserExsistException;
use zay\App\Services\ReadingServ;
use zay\App\Services\DepartmentServ;

class DepartmentCtrl extends AbstractCtrl {

  private $service;

  public function __construct() {
    $this->service = new DepartmentServ();
  }


  public function getAll() {
    return $this->service->getAll();
  }

  public function save() {
    $body = $this->getBody();
    return $this->service->save($body);
  }

  public function getById() {
    $requestUri = $_REQUEST['path'];
    $id = preg_replace('/^admin\/department\//','',$requestUri);
    return $this->service->getById($id);
  }

  public function remove() {
    $requestUri = $_REQUEST['path'];
    $id = preg_replace('/^admin\/department\//','',$requestUri);
    return $this->service->remove($id);
  }
  
}
