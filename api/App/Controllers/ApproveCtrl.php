<?php

namespace zay\App\Controllers;

use zay\App\Services\ApprovesServ;
use zay\App\Services\RequestServ;
use zay\App\Auth;
use zay\App\Services\HistoryRequestStatusServ;

class ApproveCtrl extends AbstractCtrl {

  private $service;

  public function __construct() {
    $this->service = new ApprovesServ();
  }


  public function getAll() {
    return $this->service->getAll();
  }

  public function save() {
    $body = $this->getBody();
    $approves = $this->service->getByRequestId($body['requestid']);
    if(count($approves)==0) {
      $requestService = new RequestServ();
      $request = $requestService->getById($body['requestid']);
      $request['status'] = 'SENDAPPROVE';
      $requestService->save($request);
    }
    return $this->service->save($body);
  }

  public function approve() {
    $body = $this->getBody();

    $auth = new Auth();
    $user = $auth->getCurrentUser();

    $approve = $this->service->getByRequestIdAndDepartmentId($body['requestid'], $user['departmentid']);
    $approve = $approve[0];
    $approve['userid'] = $user['id'];
    $approve['status'] = $body['status'];
    $this->service->save($approve);


    $requestApproves = $this->service->getByRequestId($approve['requestid']);
    $flag = true;
    foreach($requestApproves as $app) {
      if(!$app['userid'] || $app['status'] == 'DECLINED') {
        $flag = false;
        break;
      }
    }
    if($flag) {
      $requestService = new RequestServ();

      $request = $requestService->getById($approve['requestid']);
      $request['status'] = 'APPROVED';
      $requestService->save($request);
      $historyRequestStatusService = new HistoryRequestStatusServ();
      $historyRequestStatusService->logHistory($request['id'], $user['id'], $request['status']);
    }
    return ["status" => "all ok"];
  }


}
