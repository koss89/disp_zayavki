<?php

namespace zay\App\Controllers;

use zay\App\Services\CommentServ;
use zay\App\Services\UserServ;
use zay\App\Auth;

class CommentsCtrl extends AbstractCtrl {

  private $service;

  public function __construct() {
    $this->service = new CommentServ();
  }

  public function getAll() {
    $comments = $this->service->getAll();
    return $this->loadUsersComments($comments);
  }

  public function getByRequestId() {
    $requestUri = $_REQUEST['path'];
    $id = preg_replace('/^comment\/request\//','',$requestUri);
    $comments = $this->service->getByRequestId($id);
    return $this->loadUsersComments($comments);
  }

  public function save() {
    $body = $this->getBody();
    if(!array_key_exists('id', $body)) {
      $auth = new Auth();
      $user = $auth->getCurrentUser();
      $body['userid'] = $user['id'];
    }
    return $this->service->save($body);
  }

  public function getById() {
    $requestUri = $_REQUEST['path'];
    $id = preg_replace('/^comment\//','',$requestUri);
    return $this->service->getById($id);
  }

  public function loadUsersComments($comments) {
    $userService = new UserServ();
    foreach($comments as $key => $comm) {
      $comments[$key]['user'] = $userService->getById($comm['userid']);
    }

    return $comments;
  }

}
