<?php

namespace zay\App;

use zay\Utils\Exceptions\NotAuthException;
use zay\Utils\Exceptions\AuthValidException;
use zay\App\Services\UserServ;

class Auth {

  public function __construct() {
    session_name('SESSION');
    $session = session_start();
    if($session == FALSE) {
      throw new \Exception("Can`t start session");
    }
  }

  public function Auth($cred) {
    $userServ = new UserServ();
    if($cred["username"] && $cred["password"]) {
      try {
        $user = $userServ->getOneByField("username", $cred["username"]);
        if($user['password'] === $cred["password"] && $user['active'] === '1') {
          $_SESSION["username"] = $cred["username"];
          $_SESSION["user"] = $user;
          $_SESSION["iduser"] = $user['id'];
        } else {
          throw new NotAuthException();
        }
      } catch(\PDOException $e) {
        throw $e;
      } catch(\Exception $e) {
        throw new NotAuthException();
      }

      $_SESSION["username"] = $cred["username"];
    } else {
      throw new NotAuthException();
    }
    return $_SESSION;
  }

  public function isAuth() {
    if(array_key_exists("username",$_SESSION)) {
      if(!empty($_SESSION["username"])) {
        return true;
      }
    }
    throw new NotAuthException();
  }

  public function getCurrentUser() {
    if($this->isAuth()) {
      $userServ = new UserServ();
      $user  = $userServ->getOneByField("username", $_SESSION["username"]);
      return $user;
    }
  }

  public function isAdmin() {
    $user = $this->getCurrentUser();
    return $this->hasAuthority('ADMIN', $user);
  }
 
  public function hasAuthority($authority, $user) {
    if($user['authority']) {
      $authList = explode(',', $user['authority']);
      return in_array($authority, $authList);
    }
    return false;
  }

}
