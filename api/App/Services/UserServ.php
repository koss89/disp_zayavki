<?php

namespace zay\App\Services;

use zay\App\Services\DepartmentServ;

class UserServ extends AbstractServ {

  public function __construct() {
    parent::__construct();
    $this->table = 'user';
  }

  public function getUserWichDepartment($id) {
    $user = $this->getById($id);
    $departmentService = new DepartmentServ();
    $user['department'] = $departmentService->getById($user['departmentid']);
    return $user;
  }

}