<?php

namespace zay\App\Services;

class RequestServ extends AbstractServ {

  public function __construct() {
    parent::__construct();
    $this->table = 'request';
  }

  public function getIncoming($departmentId) {
    $sql = "SELECT request.* FROM request LEFT JOIN approves ON request.id=approves.requestid WHERE approves.userid IS null and approves.departmentid=:val";
    $params = [
      ["val", $departmentId],
    ];
    return $this->DB()->select($sql, $params);
  }

  public function find($incParams) {
    $sql = "SELECT DISTINCT(request.id), request.* FROM request LEFT JOIN approves ON request.id=approves.requestid WHERE request.dats>=:dats AND request.dats<=:datpo ";
    $params = [
      ["dats", $incParams['dats']],
      ["datpo", $incParams['datpo']],
    ];
    if(array_key_exists('status',$incParams)) {
      $sql .= " AND request.status=:status ";
      $params[] = ['status', $incParams['status']];
    }
    if(array_key_exists('department',$incParams)) {
      $sql .= "  AND request.departmentid=:department ";
      $params[] = ['department', $incParams['department']];
    }
    if(array_key_exists('tip',$incParams)) {
      $sql .= "  AND request.tip=:tip ";
      $params[] = ['tip', $incParams['tip']];
    }
    return $this->DB()->select($sql, $params);
  }

}