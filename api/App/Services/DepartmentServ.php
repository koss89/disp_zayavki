<?php

namespace zay\App\Services;

class DepartmentServ extends AbstractServ {

  public function __construct() {
    parent::__construct();
    $this->table = 'department';
  }

  public function getAll() {
    $sql = "SELECT * FROM {$this->table} ORDER BY lvl";
    return $this->DB()->select($sql);
  }

}