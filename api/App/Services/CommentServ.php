<?php

namespace zay\App\Services;

class CommentServ extends AbstractServ {

  public function __construct() {
    parent::__construct();
    $this->table = 'comment';
  }

  public function getByRequestId($requestid) {
    return $this->getAllByField('requestid', $requestid);
  }

}