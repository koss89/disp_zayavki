<?php

namespace zay\App\Services;

class HistoryRequestStatusServ extends AbstractServ {

  public function __construct() {
    parent::__construct();
    $this->table = 'historyrequeststatus';
  }

  public function getByRequestId($requestId) {
    return $this->getAllByField('requestid', $requestId);
  }

  public function getByUserId($userId) {
    return $this->getAllByField('userid', $userId);
  }

  public function logHistory($requestId, $userId, $status) {
    $history = [
      'requestid' => $requestId,
      'userid' => $userId,
      'status' => $status
    ];
    return $this->save($history);
  }


}