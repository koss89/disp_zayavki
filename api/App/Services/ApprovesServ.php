<?php

namespace zay\App\Services;

class ApprovesServ extends AbstractServ {

  public function __construct() {
    parent::__construct();
    $this->table = 'approves';
  }

  public function getByRequestId($requestId) {
    return $this->getAllByField('requestid', $requestId);
  }

  public function getByUserId($userId) {
    return $this->getAllByField('userid', $userId);
  }

  public function getByRequestIdAndDepartmentId($requestId, $departmentId) {
    $sql = "SELECT * FROM `approves` WHERE `requestid`=".$requestId." AND `departmentid`=".$departmentId;
    return $this->DB()->select($sql);
  }

}