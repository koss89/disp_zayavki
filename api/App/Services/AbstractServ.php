<?php

namespace zay\App\Services;

use zay\Utils\DB;

abstract class AbstractServ {

  private static $DB = NULL;

  protected $table;

  public function __construct() {
    if(self::$DB == NULL) {
      self::$DB = new DB();
    }
  }

  public function DB() {
    return self::$DB;
  }

  public function getOne($sql, $params) {
    $result = $this->DB()->select($sql, $params);
    if(count($result)>0) {
      return $result[0];
    }
    throw new \Exception("Not Found {$sql}");
  }

  public function getOneByField($fieldName, $fieldValue){
    $sql = "SELECT * FROM {$this->table} WHERE {$fieldName}=:val";
    $params = [
      ["val", $fieldValue],
    ];
    return $this->getOne($sql, $params);
  }

  public function getAllByField($fieldName, $fieldValue){
    $sql = "SELECT * FROM {$this->table} WHERE {$fieldName}=:val";
    $params = [
      ["val", $fieldValue],
    ];
    return $this->DB()->select($sql, $params);
  }

  public function getAll() {
    $sql = "SELECT * FROM {$this->table}";
    return $this->DB()->select($sql);
  }

  public function getById($id) {
    return $this->getOneByField('id', $id);
  }

  public function save(array $entity) {

    if(array_key_exists("id", $entity)) {
      return $this->update($entity);
    }

    return $this->insert($entity);
  }

  public function update(array $entity) {
    $id = $entity['id'];
    unset($entity['id']);
    $params = [];
    $sql = "UPDATE {$this->table} SET ";
    foreach($entity as $key => $val) {
      $sql .= " {$key}=:{$key},";
      $params[] = [ $key, $val];
    }
    $sql = substr($sql, 0, -1);
    $sql .= " WHERE id=:id";
    $params[] = [ "id", $id];

    return $this->DB()->exec($sql, $params);
  }

  public function insert(array $entity) {
    $params = [];
    $sql = "INSERT INTO {$this->table} ";
    $fields = " (";
    $values = " VALUES (";
    foreach($entity as $key => $val) {
      $fields .= " {$key},";
      $values .= " :{$key},";
      $params[] = [ $key, $val];
    }
    $fields = substr($fields, 0, -1).")";
    $values = substr($values, 0, -1).")";

    $sql .= $fields.$values;
    return $this->DB()->exec($sql, $params);
  }

  public function remove($id) {
    $params = [];
    $sql = "DELETE FROM {$this->table} WHERE id=:id";
    $params[] = [ "id", $id];

    return $this->DB()->exec($sql, $params);
  }

}
