<?php

namespace zay;

use zay\App\Auth;
use zay\Utils\Exceptions\NotAuthException;
use zay\Utils\Exceptions\AccesDenyException;

class Router {
  
  private $endpoints;

  public function __construct() {
    $this->endpoints = [
      [
        "exp" => '/^(.*)$/',
        "path" => 'AllowOriginCtrl->allow',
        "method" => "OPTIONS",
        "security" => FALSE
      ],
      //---------------------------------PUBLIC ROUTES-----------------------------
      [
        "exp" => '/^login$/',
        "path" => 'AuthCtrl->login',
        "method" => "POST",
        "security" => FALSE
      ],
      //---------------------------------PUBLIC ROUTES-----------------------------
      [
        "exp" => '/^test$/',
        "path" => 'TestCtrl->testMe',
        "method" => "GET",
        "security" => FALSE
      ],
      [
        "exp" => '/^echo$/',
        "path" => 'TestCtrl->echoMe',
        "method" => "POST",
        "security" => FALSE
      ],
      //---------------------------------PRIVATE ROUTES-----------------------------
      [
        "exp" => '/^department$/',
        "path" => 'DepartmentCtrl->getAll',
        "method" => "GET",
      ],
      [
        "exp" => '/^approve$/',
        "path" => 'ApproveCtrl->save',
        "method" => "POST",
      ],
      [
        "exp" => '/^request\/find$/',
        "path" => 'RequestCtrl->find',
        "method" => "POST"
      ],
      [
        "exp" => '/^request$/',
        "path" => 'RequestCtrl->getAll',
        "method" => "GET"
      ],
      [
        "exp" => '/^request\/incoming$/',
        "path" => 'RequestCtrl->incoming',
        "method" => "GET"
      ],
      [
        "exp" => '/^request\/incoming\/approve$/',
        "path" => 'ApproveCtrl->approve',
        "method" => "POST"
      ],
      [
        "exp" => '/^request$/',
        "path" => 'RequestCtrl->save',
        "method" => "POST"
      ],
      [
        "exp" => '/^request\//',
        "path" => 'RequestCtrl->getById',
        "method" => "GET",
      ],

      [
        "exp" => '/^comment$/',
        "path" => 'CommentsCtrl->getAll',
        "method" => "GET"
      ],
      [
        "exp" => '/^comment$/',
        "path" => 'CommentsCtrl->save',
        "method" => "POST"
      ],
      [
        "exp" => '/^comment\/request\//',
        "path" => 'CommentsCtrl->getByRequestId',
        "method" => "GET",
      ],
      [
        "exp" => '/^comment\//',
        "path" => 'CommentsCtrl->getById',
        "method" => "GET",
      ],
      [
        "exp" => '/^typework$/',
        "path" => 'TypeworkCtrl->getAll',
        "method" => "GET"
      ],
      
      //---------------------------------ADMIN ROUTES-------------------------------
      [
        "exp" => '/^admin\/user$/',
        "path" => 'UserCtrl->getAll',
        "method" => "GET",
        "authority" => "ADMIN"
      ],
      [
        "exp" => '/^admin\/user$/',
        "path" => 'UserCtrl->save',
        "method" => "POST",
        "authority" => "ADMIN"
      ],
      [
        "exp" => '/^admin\/user\//',
        "path" => 'UserCtrl->getById',
        "method" => "GET",
        "authority" => "ADMIN"
      ],
      [
        "exp" => '/^admin\/user\//',
        "path" => 'UserCtrl->removeById',
        "method" => "DELETE",
        "authority" => "ADMIN"
      ],
      //--------------------------------------
      [
        "exp" => '/^department$/',
        "path" => 'DepartmentCtrl->getAll',
        "method" => "GET",
      ],
      [
        "exp" => '/^admin\/department$/',
        "path" => 'DepartmentCtrl->save',
        "method" => "POST",
        "authority" => "ADMIN"
      ],
      [
        "exp" => '/^admin\/department\//',
        "path" => 'DepartmentCtrl->getById',
        "method" => "GET",
        "authority" => "ADMIN"
      ],
      [
        "exp" => '/^admin\/department\//',
        "path" => 'DepartmentCtrl->remove',
        "method" => "DELETE",
        "authority" => "ADMIN"
      ],
    ];
  }

  public function Route() {
    $requestUri = $_REQUEST['path'];
		foreach ( $this->endpoints as &$val ) {
      $matched = preg_match($val['exp'], $requestUri);
			if ( $matched && $_SERVER['REQUEST_METHOD'] == $val['method']) {
        //Check if nead authorization
        $this->isEndpointSecured($val);
        $path = explode("->", $val['path'], 2);
        $ControllerNamespace = "zay\App\Controllers\\".$path[0];
        $Controller = new $ControllerNamespace;

        $method = $path[1];

        return $Controller->$method();
			}
		}
		throw new \Exception('REST Endpoint not found');
  }

  public function isEndpointSecured($endpoint) {
    if(array_key_exists("security",$endpoint)) {
      if(!$endpoint["security"]) {
        return true;
      }
    }
    $auth = new Auth();
    if(!$auth->isAuth()) {
      throw new NotAuthException();
    } else {
      if(array_key_exists("authority",$endpoint)) {
        if($auth->hasAuthority($endpoint['authority'],$auth->getCurrentUser())) {
          return true;
        } else {
          throw new AccesDenyException();
        }
      }
    }
    return true;
  }



}
