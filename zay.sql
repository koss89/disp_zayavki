-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: softnoe
-- Время создания: Апр 28 2020 г., 05:14
-- Версия сервера: 10.1.44-MariaDB
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `zay`
--

-- --------------------------------------------------------

--
-- Структура таблицы `approves`
--

CREATE TABLE `approves` (
  `id` bigint(20) NOT NULL,
  `userid` bigint(20) DEFAULT NULL,
  `requestid` bigint(20) NOT NULL,
  `departmentid` bigint(20) NOT NULL,
  `status` enum('CREATED','APPROVED','DECLINED') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'CREATED',
  `dat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE `comment` (
  `id` bigint(20) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userid` bigint(20) NOT NULL,
  `requestid` bigint(20) NOT NULL,
  `comment` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('REQUEST') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'REQUEST'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `department`
--

CREATE TABLE `department` (
  `id` bigint(20) NOT NULL,
  `nam` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `lvl` int(11) NOT NULL DEFAULT '100'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `department`
--

INSERT INTO `department` (`id`, `nam`, `lvl`) VALUES
(1, 'Філія Арбузинського району', 100),
(2, 'Філія Баштанського району', 100),
(3, 'Філія Березанського району', 100),
(4, 'Філія Березнегуватського району', 100),
(5, 'Філія Братського району', 100),
(6, 'Філія Миколаївського району', 100),
(7, 'Філія Веселинівського району', 100),
(8, 'Філія Вознесенського району', 100),
(9, 'Філія Врадіївського району', 100),
(10, 'Філія Доманівського району', 100),
(11, 'Філія Єланецького району', 100),
(12, 'Філія Казанківського району', 100),
(13, 'Філія Кривоозерського району', 100),
(14, 'Філія м. Миколаїв', 100),
(15, 'Філія Новоодеського району', 100),
(16, 'Філія Новобузького району', 100),
(17, 'Філія Очаківського району', 100),
(18, 'Філія Первомайського району', 100),
(19, 'Філія Південна', 100),
(20, 'Філія Снігурівського району', 100);

-- --------------------------------------------------------

--
-- Структура таблицы `historyrequeststatus`
--

CREATE TABLE `historyrequeststatus` (
  `id` bigint(20) NOT NULL,
  `requestid` bigint(20) NOT NULL,
  `userid` bigint(20) DEFAULT NULL,
  `status` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `request`
--

CREATE TABLE `request` (
  `id` bigint(20) NOT NULL,
  `tip` bigint(20) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dats` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datpo` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fakts` timestamp NULL DEFAULT NULL,
  `faktpo` timestamp NULL DEFAULT NULL,
  `equipment` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `ps` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid` bigint(20) NOT NULL,
  `status` enum('OPEN','APPROVED','DECLINED','CREATED','SENDAPPROVE','CLOSED','WORKFINISH') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'CREATED',
  `departmentid` bigint(20) NOT NULL,
  `typeworkid` bigint(20) DEFAULT NULL,
  `tehzah` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `previewrequestid` bigint(20) DEFAULT NULL,
  `narad` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `typework`
--

CREATE TABLE `typework` (
  `id` bigint(20) NOT NULL,
  `nam` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `typework`
--

INSERT INTO `typework` (`id`, `nam`) VALUES
(1, 'Поточний ремонт'),
(2, 'Капітальний ремонт'),
(3, 'Реконструкція'),
(4, 'Демонтаж');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `username` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `fio` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `authority` set('USER','ADMIN') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'USER',
  `departmentid` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `fio`, `active`, `authority`, `departmentid`) VALUES
(1, 'admin', 'admin', 'admin admin admin1', 1, 'USER,ADMIN', 1),
(4, 'test', 'test', 'tdtdd', 1, 'USER', 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `approves`
--
ALTER TABLE `approves`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `historyrequeststatus`
--
ALTER TABLE `historyrequeststatus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `typework`
--
ALTER TABLE `typework`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `approves`
--
ALTER TABLE `approves`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `comment`
--
ALTER TABLE `comment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `department`
--
ALTER TABLE `department`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `historyrequeststatus`
--
ALTER TABLE `historyrequeststatus`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `request`
--
ALTER TABLE `request`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `typework`
--
ALTER TABLE `typework`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
